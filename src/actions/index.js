// Action Creator
export const selectSong = song => {
  // Return an ation
  return {
    type: "SONG_SELECTED",
    payload: song
  };
};
